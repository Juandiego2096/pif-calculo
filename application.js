var object = null;

function getXCoordinate(value) {
	return value * 20;
}

function getYCoordinate(value) {
	return value * -20;
}

$("#btnProcesar").click(function() {
	//clear_canvas();
	var puntoA = $("#puntoA").val().split(",");
	var puntoB = $("#puntoB").val().split(",");
	var colorPunto = $("#colorPunto").val();
	var colorLinea = $("#colorLinea").val();

	if(validateValues(puntoA[0], puntoA[1], puntoB[0], puntoB[1])) {
		object = {
			puntoA: {
				puntoX: getXCoordinate(puntoA[0]),
				puntoY: getYCoordinate(puntoA[1])
			},
			puntoB: {
				puntoX: getXCoordinate(puntoB[0]),
				puntoY: getYCoordinate(puntoB[1])
			}
		};
		addPoint(puntoA[0], puntoA[1], colorPunto);
		addPoint(puntoB[0], puntoB[1], colorPunto);
		addLine(puntoA[0], puntoA[1], puntoB[0], puntoB[1], colorLinea);

		var pendiente = getM(puntoA[0], puntoB[0], puntoA[1], puntoB[1]);
		var value = getEquation(puntoA[0], puntoA[1], pendiente);
		
		$("#ecuacion").html(value);
		$("#pendiente").html("m= " + pendiente);
	}
	else {
		$("#modalButton").click();
	}
});

function validateValues(aX,aY, bX, bY) {
	var response = true;
	if(aY > 14 || bY > 14) {
		response = false;
	}

	if(aY < -15 || bY < -15) {
		response = false;
	}

	if(aX > 17 || bX > 17) {
		response = false;
	}

	if(aX < -17 || bX < -17) {
		response = false;
	}

	return response;
}

function addPoint(x, y, color) {
	if (canvas && canvas.getContext) {
		
		if(ctx) {
			ctx.strokeStyle = color;
			ctx.beginPath();
			ctx.arc(getXCoordinate(x),getYCoordinate(y),2,0,2*Math.PI);
			ctx.closePath();
	      	ctx.fill();
			ctx.stroke();
		}
	}
}

function addLine(aX, aY, bX, bY, color) {
	if (canvas && canvas.getContext) {
		
		if (ctx) {
			ctx.lineWidth = 2;
			ctx.strokeStyle = color;
			ctx.beginPath();
			ctx.moveTo(getXCoordinate(bX), getYCoordinate(bY));
			ctx.lineTo(getXCoordinate(aX), getYCoordinate(aY));
			ctx.stroke();
			 
		}
	}
}

// Conclusión
(function() {
  /**
   * Ajuste decimal de un número.
   *
   * @param {String}  tipo  El tipo de ajuste.
   * @param {Number}  valor El numero.
   * @param {Integer} exp   El exponente (el logaritmo 10 del ajuste base).
   * @returns {Number} El valor ajustado.
   */
  function decimalAdjust(type, value, exp) {
    // Si el exp no está definido o es cero...
    if (typeof exp === 'undefined' || +exp === 0) {
      return Math[type](value);
    }
    value = +value;
    exp = +exp;
    // Si el valor no es un número o el exp no es un entero...
    if (isNaN(value) || !(typeof exp === 'number' && exp % 1 === 0)) {
      return NaN;
    }
    // Shift
    value = value.toString().split('e');
    value = Math[type](+(value[0] + 'e' + (value[1] ? (+value[1] - exp) : -exp)));
    // Shift back
    value = value.toString().split('e');
    return +(value[0] + 'e' + (value[1] ? (+value[1] + exp) : exp));
  }

  // Decimal round
  if (!Math.round10) {
    Math.round10 = function(value, exp) {
      return decimalAdjust('round', value, exp);
    };
  }
  // Decimal floor
  if (!Math.floor10) {
    Math.floor10 = function(value, exp) {
      return decimalAdjust('floor', value, exp);
    };
  }
  // Decimal ceil
  if (!Math.ceil10) {
    Math.ceil10 = function(value, exp) {
      return decimalAdjust('ceil', value, exp);
    };
  }
})();

function getM(x1, x2, y1, y2) {
	var topValue = y2 - y1;
	var bottomValue = x2 - x1;

	value = topValue / bottomValue;

	return Math.floor10(value,-1);
}

function getEquation(x1, y1, m) {
	y1 = -(y1);
	x1 = m * -x1;
	y1 = y1 * -1;

	var c = x1 + y1;
	if(c >= 0)
		c = "+" + c;

	var result = "y = " + m + "x " + c;
	return result;
}

window.onload = function() {
	$("#modalButton").click();
}